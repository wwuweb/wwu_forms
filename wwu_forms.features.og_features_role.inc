<?php
/**
 * @file
 * wwu_forms.features.og_features_role.inc
 */

/**
 * Implements hook_og_features_default_roles().
 */
function wwu_forms_og_features_default_roles() {
  $roles = array();

  // Exported OG Role: 'node:group:webform administrator'.
  $roles['node:group:webform administrator'] = array(
    'gid' => 0,
    'group_type' => 'node',
    'group_bundle' => 'group',
    'name' => 'webform administrator',
  );

  // Exported OG Role: 'node:group:webform creator'.
  $roles['node:group:webform creator'] = array(
    'gid' => 0,
    'group_type' => 'node',
    'group_bundle' => 'group',
    'name' => 'webform creator',
  );

  // Exported OG Role: 'node:group:webfrom reviewer'.
  $roles['node:group:webfrom reviewer'] = array(
    'gid' => 0,
    'group_type' => 'node',
    'group_bundle' => 'group',
    'name' => 'webfrom reviewer',
  );

  return $roles;
}
