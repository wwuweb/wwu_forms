<?php
/**
 * @file
 * wwu_forms.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function wwu_forms_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'access all webform results'.
  $permissions['access all webform results'] = array(
    'name' => 'access all webform results',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'webform',
  );

  // Exported permission: 'access own webform results'.
  $permissions['access own webform results'] = array(
    'name' => 'access own webform results',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'webform',
  );

  // Exported permission: 'access own webform submissions'.
  $permissions['access own webform submissions'] = array(
    'name' => 'access own webform submissions',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'webform',
  );

  // Exported permission: 'access webform reports'.
  $permissions['access webform reports'] = array(
    'name' => 'access webform reports',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'webform_report',
  );

  // Exported permission: 'create group content'.
  $permissions['create group content'] = array(
    'name' => 'create group content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'create webform content'.
  $permissions['create webform content'] = array(
    'name' => 'create webform content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'create webform reports'.
  $permissions['create webform reports'] = array(
    'name' => 'create webform reports',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'webform_report',
  );

  // Exported permission: 'create webform_report content'.
  $permissions['create webform_report content'] = array(
    'name' => 'create webform_report content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'delete all webform submissions'.
  $permissions['delete all webform submissions'] = array(
    'name' => 'delete all webform submissions',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'webform',
  );

  // Exported permission: 'delete any group content'.
  $permissions['delete any group content'] = array(
    'name' => 'delete any group content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'delete any webform content'.
  $permissions['delete any webform content'] = array(
    'name' => 'delete any webform content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'delete any webform_report content'.
  $permissions['delete any webform_report content'] = array(
    'name' => 'delete any webform_report content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'delete own group content'.
  $permissions['delete own group content'] = array(
    'name' => 'delete own group content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'delete own webform content'.
  $permissions['delete own webform content'] = array(
    'name' => 'delete own webform content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'delete own webform submissions'.
  $permissions['delete own webform submissions'] = array(
    'name' => 'delete own webform submissions',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'webform',
  );

  // Exported permission: 'delete own webform_report content'.
  $permissions['delete own webform_report content'] = array(
    'name' => 'delete own webform_report content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'edit all webform submissions'.
  $permissions['edit all webform submissions'] = array(
    'name' => 'edit all webform submissions',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'webform',
  );

  // Exported permission: 'edit any group content'.
  $permissions['edit any group content'] = array(
    'name' => 'edit any group content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'edit any webform content'.
  $permissions['edit any webform content'] = array(
    'name' => 'edit any webform content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'edit any webform_report content'.
  $permissions['edit any webform_report content'] = array(
    'name' => 'edit any webform_report content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'edit own group content'.
  $permissions['edit own group content'] = array(
    'name' => 'edit own group content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'edit own webform content'.
  $permissions['edit own webform content'] = array(
    'name' => 'edit own webform content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'edit own webform reports'.
  $permissions['edit own webform reports'] = array(
    'name' => 'edit own webform reports',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'webform_report',
  );

  // Exported permission: 'edit own webform submissions'.
  $permissions['edit own webform submissions'] = array(
    'name' => 'edit own webform submissions',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'webform',
  );

  // Exported permission: 'edit own webform_report content'.
  $permissions['edit own webform_report content'] = array(
    'name' => 'edit own webform_report content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'edit webform reports'.
  $permissions['edit webform reports'] = array(
    'name' => 'edit webform reports',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'webform_report',
  );

  // Exported permission: 'use php code'.
  $permissions['use php code'] = array(
    'name' => 'use php code',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'webform_report',
  );

  return $permissions;
}
