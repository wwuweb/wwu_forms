<?php
/**
 * @file
 * wwu_forms.features.og_features_permission.inc
 */

/**
 * Implements hook_og_features_default_permissions().
 */
function wwu_forms_og_features_default_permissions() {
  $permissions = array();

  // Exported og permission: 'node:group:access all webform results'
  $permissions['node:group:access all webform results'] = array(
    'roles' => array(
      'webform administrator' => 'webform administrator',
      'webfrom reviewer' => 'webfrom reviewer',
    ),
  );

  // Exported og permission: 'node:group:access own webform submissions'
  $permissions['node:group:access own webform submissions'] = array(
    'roles' => array(
      'webform administrator' => 'webform administrator',
      'webform creator' => 'webform creator',
    ),
  );

  // Exported og permission: 'node:group:add user'
  $permissions['node:group:add user'] = array(
    'roles' => array(
      'webform administrator' => 'webform administrator',
    ),
  );

  // Exported og permission: 'node:group:administer group'
  $permissions['node:group:administer group'] = array(
    'roles' => array(
      'webform administrator' => 'webform administrator',
    ),
  );

  // Exported og permission: 'node:group:approve and deny subscription'
  $permissions['node:group:approve and deny subscription'] = array(
    'roles' => array(
      'webform administrator' => 'webform administrator',
    ),
  );

  // Exported og permission: 'node:group:create webform content'
  $permissions['node:group:create webform content'] = array(
    'roles' => array(
      'webform administrator' => 'webform administrator',
      'webform creator' => 'webform creator',
    ),
  );

  // Exported og permission: 'node:group:delete all webform submissions'
  $permissions['node:group:delete all webform submissions'] = array(
    'roles' => array(
      'webform administrator' => 'webform administrator',
    ),
  );

  // Exported og permission: 'node:group:delete any webform content'
  $permissions['node:group:delete any webform content'] = array(
    'roles' => array(
      'webform administrator' => 'webform administrator',
    ),
  );

  // Exported og permission: 'node:group:delete own webform content'
  $permissions['node:group:delete own webform content'] = array(
    'roles' => array(
      'webform administrator' => 'webform administrator',
    ),
  );

  // Exported og permission: 'node:group:delete own webform submissions'
  $permissions['node:group:delete own webform submissions'] = array(
    'roles' => array(
      'webform administrator' => 'webform administrator',
    ),
  );

  // Exported og permission: 'node:group:edit all webform submissions'
  $permissions['node:group:edit all webform submissions'] = array(
    'roles' => array(
      'webform administrator' => 'webform administrator',
      'webform creator' => 'webform creator',
    ),
  );

  // Exported og permission: 'node:group:edit own webform submissions'
  $permissions['node:group:edit own webform submissions'] = array(
    'roles' => array(
      'webform administrator' => 'webform administrator',
      'webform creator' => 'webform creator',
    ),
  );

  // Exported og permission: 'node:group:manage members'
  $permissions['node:group:manage members'] = array(
    'roles' => array(
      'webform administrator' => 'webform administrator',
    ),
  );

  // Exported og permission: 'node:group:manage permissions'
  $permissions['node:group:manage permissions'] = array(
    'roles' => array(
      'webform administrator' => 'webform administrator',
    ),
  );

  // Exported og permission: 'node:group:manage roles'
  $permissions['node:group:manage roles'] = array(
    'roles' => array(
      'webform administrator' => 'webform administrator',
    ),
  );

  // Exported og permission: 'node:group:subscribe'
  $permissions['node:group:subscribe'] = array(
    'roles' => array(),
  );

  // Exported og permission: 'node:group:subscribe without approval'
  $permissions['node:group:subscribe without approval'] = array(
    'roles' => array(),
  );

  // Exported og permission: 'node:group:unsubscribe'
  $permissions['node:group:unsubscribe'] = array(
    'roles' => array(),
  );

  // Exported og permission: 'node:group:update any webform content'
  $permissions['node:group:update any webform content'] = array(
    'roles' => array(
      'webform administrator' => 'webform administrator',
      'webform creator' => 'webform creator',
    ),
  );

  // Exported og permission: 'node:group:update body field'
  $permissions['node:group:update body field'] = array(
    'roles' => array(
      'webform administrator' => 'webform administrator',
      'webform creator' => 'webform creator',
    ),
  );

  // Exported og permission: 'node:group:update group'
  $permissions['node:group:update group'] = array(
    'roles' => array(
      'webform administrator' => 'webform administrator',
    ),
  );

  // Exported og permission: 'node:group:update group_group field'
  $permissions['node:group:update group_group field'] = array(
    'roles' => array(
      'webform administrator' => 'webform administrator',
    ),
  );

  // Exported og permission: 'node:group:update og_group_ref field'
  $permissions['node:group:update og_group_ref field'] = array(
    'roles' => array(
      'webform administrator' => 'webform administrator',
      'webform creator' => 'webform creator',
    ),
  );

  // Exported og permission: 'node:group:update og_user_node field'
  $permissions['node:group:update og_user_node field'] = array(
    'roles' => array(
      'webform administrator' => 'webform administrator',
    ),
  );

  // Exported og permission: 'node:group:update own webform content'
  $permissions['node:group:update own webform content'] = array(
    'roles' => array(
      'webform administrator' => 'webform administrator',
      'webform creator' => 'webform creator',
    ),
  );

  // Exported og permission: 'node:group:view body field'
  $permissions['node:group:view body field'] = array(
    'roles' => array(
      'webform administrator' => 'webform administrator',
      'webform creator' => 'webform creator',
    ),
  );

  // Exported og permission: 'node:group:view group_group field'
  $permissions['node:group:view group_group field'] = array(
    'roles' => array(
      'webform administrator' => 'webform administrator',
    ),
  );

  // Exported og permission: 'node:group:view og_group_ref field'
  $permissions['node:group:view og_group_ref field'] = array(
    'roles' => array(
      'webform administrator' => 'webform administrator',
      'webform creator' => 'webform creator',
    ),
  );

  // Exported og permission: 'node:group:view og_user_node field'
  $permissions['node:group:view og_user_node field'] = array(
    'roles' => array(
      'webform administrator' => 'webform administrator',
    ),
  );

  return $permissions;
}
